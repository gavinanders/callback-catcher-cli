# README #

### What is Call Back Catcher? ###

Call Back Catcher is a simple tool based on BurpSuite's collaborator agent. It detects any incoming traffic and logs the connection details. 
Catcher affectivly acts like a honeypot and opens up a number of listeners on a user-defined list of ports. Upon connection from the client
the handler attempts to detect the protocol being used and logs it to stdout as a hexdump. CallbackCatcher is also capable of detecting 'tokens'
or unique values from the incoming request. This will help when trying to detect certain incoming requests.  

The tool is written in Python and based on the ServerSocket.TcpServer module. The tool works off a set of plugins for each opened port. See
handlers/simple.py for an example of a very simple plugin.

### Plugins/Supported services ###
The following services list some of the service handlers that can be spun up. All of which can be wrapped in SSL
* ftp
* smtp
* telnet
* postgres
* mysql
* dns
* socket forwarding

### Requirements ###
* Python 2.7 
* hexdump
* dnslib
(possibly more, depending on what the handlers are coded to do)

### Installation, Setup and Running ###

First off you need a "clean" IP. This means a relatvily static IP with as few a ports open as possible. This ensures that the majority of 
traffic will be caught by the listeners and not hit ports that are in active use. The catcher will not open up a port if something is already
running on it (derr!).

1. Clone this repository to your 'clean' server
2. Modify the .settings/services.xml to the desired ports to listen on.
3. Modify the .settings/config.ini to use the correct external IP, will work on 0.0.0.0 (but you may not want that)
4. `python catcher.py -v`
5. Results are stored within a log or to stdout

### TODO ###
* Add support for IPv6
* Create dns rebind plugin
* Create responder wrapper
* Create configurable DNS plugin

### Example ###

```
$ python catcher.py --help
Usage: catcher.py [OPTIONS] (args)

Options:
  --version             show program's version number and exit
  -h, --help            show this help message and exit
  -c CONFIG, --config=CONFIG
                        load details from config.ini
  -t, --token           add a value to search for in each callback. Default is
                        randomly generated token
  -f, --flush           remove all tokens, regenerate SSL certificates
  -v, --verbose         outputs logging to stdout
  
$sudo python catcher.py -v
sudo: unable to resolve host GAV-PC

   _____      _ _ ____             _     _____      _       _
  / ____|    | | |  _ \           | |   / ____|    | |     | |
 | |     __ _| | | |_) | __ _  ___| | _| |     __ _| |_ ___| |__   ___ _ __
 | |    / _` | | |  _ < / _` |/ __| |/ / |    / _` | __/ __| '_ \ / _ \ '__|
 | |___| (_| | | | |_) | (_| | (__|   <| |___| (_| | || (__| | | |  __/ |
  \_____\__,_|_|_|____/ \__,_|\___|_|\_\_____\__,_|\__\___|_| |_|\___|_|

This tool is a glorified honeypot to detect and record out-of-band connect backs
during a pentest. Based on Python's TCPServer module.

[+] CallBackCatcher CLI Version:        0.1
[+] Python Version:             2.7.6 (default, Oct 26 2016, 20:30:19)
[GCC 4.8.4]
[+] Using the default config.ini. Have you set the listening IP address?
[+] Config:             ok!
[+] Verbose turned on
[+] Debugging log:      debug.log
[+] Service file:       ./.settings/services.xml
[+] Fingerprint file:   ./.settings/fingerprints.xml
[+] Tokens file:        ./.settings/tokens
[+] Services enabled:   3 of 3
[+] Fingerprints:       99

[+] Starting catcher on 192.168.56.1
[+] Opening socket on 80/tcp    - No handler
[+] Opening socket on 443/tcp   - No handler
[+] Opening socket on 53/udp    - custom handler 'dns.py'
[+] Total running ports: 3
[+] DEBUG - setup()
[+] DEBUG - setup()
[+] DEBUG - handler()
[+] DEBUG - setup()
[+] DEBUG - handler()
[+] DEBUG - base_handler()
[+] DEBUG - handler()
[+] DEBUG - base_handler()
[+] INFO - Source       192.168.56.1:35586
[+] DEBUG - base_handler()
[+] INFO - Destination  192.168.56.1:80 (TCP)
[+] INFO - Fingerprint  HTTP GET Request
[+] INFO - Time         2017-11-29 18:14:31.729362
[+] INFO - Data size    491 (bytes)
[+] INFO - Token found  FALSE
[+] INFO - Data
00000000: 47 45 54 20 2F 20 48 54  54 50 2F 31 2E 31 0D 0A  GET / HTTP/1.1..
00000010: 48 6F 73 74 3A 20 31 39  32 2E 31 36 38 2E 35 36  Host: 192.168.56
00000020: 2E 31 0D 0A 43 6F 6E 6E  65 63 74 69 6F 6E 3A 20  .1..Connection:
00000030: 6B 65 65 70 2D 61 6C 69  76 65 0D 0A 43 61 63 68  keep-alive..Cach
00000040: 65 2D 43 6F 6E 74 72 6F  6C 3A 20 6D 61 78 2D 61  e-Control: max-a
00000050: 67 65 3D 30 0D 0A 55 73  65 72 2D 41 67 65 6E 74  ge=0..User-Agent
00000060: 3A 20 4D 6F 7A 69 6C 6C  61 2F 35 2E 30 20 28 57  : Mozilla/5.0 (W
00000070: 69 6E 64 6F 77 73 20 4E  54 20 31 30 2E 30 3B 20  indows NT 10.0;
00000080: 57 69 6E 36 34 3B 20 78  36 34 29 20 41 70 70 6C  Win64; x64) Appl
00000090: 65 57 65 62 4B 69 74 2F  35 33 37 2E 33 36 20 28  eWebKit/537.36 (
000000A0: 4B 48 54 4D 4C 2C 20 6C  69 6B 65 20 47 65 63 6B  KHTML, like Geck
000000B0: 6F 29 20 43 68 72 6F 6D  65 2F 36 32 2E 30 2E 33  o) Chrome/62.0.3
000000C0: 32 30 32 2E 39 34 20 53  61 66 61 72 69 2F 35 33  202.94 Safari/53
000000D0: 37 2E 33 36 0D 0A 55 70  67 72 61 64 65 2D 49 6E  7.36..Upgrade-In
000000E0: 73 65 63 75 72 65 2D 52  65 71 75 65 73 74 73 3A  secure-Requests:
000000F0: 20 31 0D 0A 41 63 63 65  70 74 3A 20 74 65 78 74   1..Accept: text
00000100: 2F 68 74 6D 6C 2C 61 70  70 6C 69 63 61 74 69 6F  /html,applicatio
00000110: 6E 2F 78 68 74 6D 6C 2B  78 6D 6C 2C 61 70 70 6C  n/xhtml+xml,appl
00000120: 69 63 61 74 69 6F 6E 2F  78 6D 6C 3B 71 3D 30 2E  ication/xml;q=0.
00000130: 39 2C 69 6D 61 67 65 2F  77 65 62 70 2C 69 6D 61  9,image/webp,ima
00000140: 67 65 2F 61 70 6E 67 2C  2A 2F 2A 3B 71 3D 30 2E  ge/apng,*/*;q=0.
00000150: 38 0D 0A 41 63 63 65 70  74 2D 45 6E 63 6F 64 69  8..Accept-Encodi
00000160: 6E 67 3A 20 67 7A 69 70  2C 20 64 65 66 6C 61 74  ng: gzip, deflat
00000170: 65 0D 0A 41 63 63 65 70  74 2D 4C 61 6E 67 75 61  e..Accept-Langua
00000180: 67 65 3A 20 65 6E 2D 47  42 2C 65 6E 2D 55 53 3B  ge: en-GB,en-US;
00000190: 71 3D 30 2E 39 2C 65 6E  3B 71 3D 30 2E 38 0D 0A  q=0.9,en;q=0.8..
000001A0: 43 6F 6F 6B 69 65 3A 20  42 43 53 49 2D 43 53 2D  Cookie: BCSI-CS-
000001B0: 34 30 36 64 35 39 39 38  63 63 31 63 62 39 37 37  406d5998cc1cb977
000001C0: 3D 32 0D 0A 0D 0A                                 =2....
[+] DEBUG - finish()
[+] INFO - Connection closed
[+] INFO - Source       192.168.56.1:35588
[+] INFO - Destination  192.168.56.1:80 (TCP)
[+] INFO - Fingerprint  HTTP GET Request
[+] INFO - Time         2017-11-29 18:14:32.176301
[+] INFO - Data size    431 (bytes)
[+] INFO - Token found  FALSE
[+] INFO - Data
[+] DEBUG - finish()
[+] INFO - Connection closed
[+] WARNING - 192.168.56.1:35589 Connection timeout
[+] DEBUG - finish()
[+] INFO - Connection closed
[+] DEBUG - setup()
[+] DEBUG - handler()
[+] DEBUG - base_handler()
[+] INFO - Source       192.168.56.1:35600
[+] INFO - Destination  192.168.56.1:80 (TCP)
[+] INFO - Fingerprint  UNKNOWN
[+] INFO - Time         2017-11-29 18:15:54.568068
[+] INFO - Data size    43 (bytes)
[+] INFO - Token found  FALSE
[+] INFO - Data
00000000: 68 65 6C 6C 6F 0A                                 hello.
[+] DEBUG - finish()
[+] INFO - Connection closed
^C[+] ^C received, shutting down server
```