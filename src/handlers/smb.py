'''
Created on 15 Sep 2017

@author: gavin
'''

from basehandler import TcpHandler
from impacket import nmb
from impacket import smb as smb1
from impacket import smb3structs as smb2

class smb(TcpHandler):
    '''
    Handles incoming connections and keeps it open
    '''

    def __init__(self, *args):
        '''
        Constructor
        '''
        self.timeout = 60*5
        TcpHandler.__init__(self, *args)
        
    def base_handle(self):
        print self.client_address
        while True:
            try:
                data = self.handle_one_request()
                self.request.settimeout(1)
                if not data:
                    break
                
                sessionreq = nmb.NetBIOSSessionPacket(data)
                if sessionreq.get_type() == nmb.NETBIOS_SESSION_REQUEST:
                    _, rn, my = sessionreq.get_trailer().split(' ')
                    remote_name = nmb.decode_name('\x20' + rn)
                    clientname = nmb.decode_name('\x20' + my) 
                    print "NetBIOS Session request from (%s, %s, %s)" % (self.client_address[0], remote_name[1].strip(), clientname[1].strip())
                    
                    #respond with positive session response - NETBIOS_SESSION_POSITIVE_RESPONSE
                    sessionresp = nmb.NetBIOSSessionPacket()
                    sessionresp.set_type(nmb.NETBIOS_SESSION_POSITIVE_RESPONSE)
                    sessionresp.set_trailer(sessionreq.get_trailer())
                    self.request.send(sessionresp.rawData())
                                    
                    try:
                        data = self.handle_one_request()[4:]
                        smbpacket = smb1.NewSMBPacket(data=data)
                        print smbpacket.structure
                        
                    except Exception, e:
                        print e
                        pass
                        #packet = smb2.SMB2Packet(data=data)
                        
            except Exception, e:
                print e
                break
        
        
        