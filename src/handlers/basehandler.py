'''
Created on 24 Aug 2017

@author: gavin

https://github.com/fabio-d/honeypot/blob/master/main.py
'''
from SocketServer import BaseRequestHandler
from hexdump import hexdump
from datetime import datetime
from ssl import SSLError

import logging
import sys
import socket

logger = logging.getLogger('callback')

class HandlerUtilsMixin(object):
    def _log(self, address, protocol, fingerprint, data, foundtoken):
        '''
        writes findings to stdout
        '''
        #Record to database
        client = "%s:%s" % (address[0],address[1])
        timestamp = str(datetime.now())
        datasize = sys.getsizeof(data)
        server = "%s:%s" % (self.server.server_address[0],self.server.server_address[1])
    
        #Aquire lock here to write to stdout correctly
        self.server.lock.acquire()
        try:            
            if self.server.options.VERBOSE is True:
                logger.info("Source       %s" % client)
                logger.info("Destination  %s (%s)" % (server, protocol))
                logger.info("Fingerprint  %s" % fingerprint)
                logger.info("Time         %s" % timestamp)
                logger.info("Data size    %s (bytes)" % datasize)
                logger.info("Token found  %s" % str(foundtoken).upper())
                dump = "Data\n" + hexdump(data, result='return')
                logger.info(dump)
        finally:
            self.server.lock.release()
    
    def _check_for_token(self, data):
        '''
        checks for a token within raw data
        for now lets assume all tokens are good old strings
        wanted! process hex values
        :return True or False
        '''
        for t in self.server.tokens:
            if str(t.strip()).encode('hex') in data.encode('hex'):
                return True
        return False        
    
    def _fingerprint(self, data):
        '''
        loads the probes into memory and returns the name if one is found
        encode both to hex
        '''
        fingerprint = 'UNKNOWN'
        for f in self.server.fingerprints:
            name = f['name']
            raw = f['probe'].decode('unicode-escape')
            probe = "".join([hex(ord(c))[2:].zfill(2) for c in raw])
            sample = data.encode('hex')[:len(probe)]
            
            if sample == probe:
                fingerprint = name
                break
        return fingerprint.strip()

class TcpHandler(HandlerUtilsMixin, BaseRequestHandler):
    '''
    Listener for raw TCP socket
    '''
    SERVER_PROTOCOL = 'TCP'
    TIMEOUT = 10
    BUFFER_SIZE = 1024
   
    def __init__(self, *args):
        '''
        Constructor
        '''
        BaseRequestHandler.__init__(self, *args)
        
    def setup(self):
        logger.debug("setup()")
        self.request.settimeout(TcpHandler.TIMEOUT)
        
    def handle(self):
        logger.debug("handler()")
        try:
            self.base_handle()
        except socket.timeout:
            self.handle_timeout()
        except SSLError as e:
            #SSLError: The read operation timed out
            logger.error(e)
        except Exception as e:
            if 'The read operation timed out' in e.args:
                self.handle_timeout()
            elif 'Connection reset by peer' in e.args:
                logger.warning("Client closed the connection")
            else:
                raise
        
    def base_handle(self):
        '''
        overwrite this method
        '''
        logger.debug("base_handler()")
        self.request.send(b'CallBackCatcher CLI online\r\n')
        data = self.handle_one_request()    #dont do anything with it by default
        
    def finish(self):
        '''
        finish up function
        '''
        logger.debug("finish()")
        logger.info("Connection closed")
        
    def handle_one_request(self):
        '''
        handles the next incoming request in the buffer
        does not account for multiple parts being sent
        '''
        packet = self.request.recv(self.BUFFER_SIZE)
        if packet is not None:
            self.log_request(packet)
            return packet
        
    def log_request(self, data):
        '''
        handles one request and adds to data logger
        '''
        if data is not None:
            #Fingerprint the data 
            fingerprint = 'NO DATA'
            fingerprint = self._fingerprint(data)
            
            #Check for a token in the data
            foundtoken = False
            if len(self.server.tokens) > 0:
                self._check_for_token(data)
                
            self._log(self.client_address, self.SERVER_PROTOCOL, fingerprint, data, foundtoken)
        return
    
    def handle_error(self, request, client_address):
        self.server.lock.aquire()
        logger.error("%s:%i Connection error" % (self.client_address[0], self.client_address[1]))
        self.request.close()
        self.server.lock.release()
        
    def handle_timeout(self):
        self.server.lock.acquire()
        logger.warning("%s:%i connection timed out" % (self.client_address[0], self.client_address[1]))
        self.server.lock.release()
        
class UdpHandler(HandlerUtilsMixin, BaseRequestHandler):
    '''
    Listener for raw UDP socket
    '''
    SERVER_PROTOCOL = 'UDP'
    TIMEOUT = 30
    BUFFER_SIZE = 2048
    
    def __init__(self, *args):
        '''
        Constructor
        '''
        BaseRequestHandler.__init__(self, *args)
        
    def setup(self):
        logger.debug("udp_setup()")
        #self.request.settimeout(self.TIMEOUT)
        
    def handle(self):
        logger.debug("udp_handle()")
        try:
            self.base_handle()
        except socket.timeout:
            self.handle_timeout()
        except SSLError as e:
            #SSLError: The read operation timed out
            print "SSL Error"
        except Exception as e:
            if 'The read operation timed out' in e.args:
                self.handle_timeout()
            elif 'Connection reset by peer' in e.args:
                logger.warning("Client closed the connection")
            else:
                raise
        
    def base_handle(self):
        '''
        overwrite this method
        '''
        logger.debug("udp_base_handler()")
        data = self.handle_one_request()    #dont do anything with it by default
        
    def finish(self):
        '''
        finish up function
        '''
        logger.debug("udp_finish()")
        logger.info("Connection closed")
        
    def handle_one_request(self):
        '''
        handles the next incoming request in the buffer
        does not account for multiple parts being sent
        '''
        socket = self.request[1]
        packet = self.request[0].rstrip()
        if packet is not None:
            self.log_request(packet)
        return (socket, packet)
        
    def log_request(self, data):
        '''
        handles one request and adds to data logger
        '''
        if data is not None:
            #Fingerprint the data 
            fingerprint = 'NO DATA'
            fingerprint = self._fingerprint(data)
            
            #Check for a token in the data
            foundtoken = False
            if len(self.server.tokens) > 0:
                self._check_for_token(data)
                
            self._log(self.client_address, self.SERVER_PROTOCOL, fingerprint, data, foundtoken)
        return
    
    def handle_error(self, request, client_address):
        self.server.lock.aquire()
        logger.error("%s:%i Connection error" % (self.client_address[0], self.client_address[1]))
        self.request.close()
        self.server.lock.release()
        
    def handle_timeout(self):
        self.server.lock.acquire()
        logger.warning("%s:%i connection timed out" % (self.client_address[0], self.client_address[1]))
        self.server.lock.release()