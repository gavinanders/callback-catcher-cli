'''
Created on 24 Aug 2017

@author: gavin
'''
#!/usr/bin/python -tt

__version__='$Revision: 0.1 $'[11:-2]

NAME = """
   _____      _ _ ____             _     _____      _       _               
  / ____|    | | |  _ \           | |   / ____|    | |     | |              
 | |     __ _| | | |_) | __ _  ___| | _| |     __ _| |_ ___| |__   ___ _ __ 
 | |    / _` | | |  _ < / _` |/ __| |/ / |    / _` | __/ __| '_ \ / _ \ '__|
 | |___| (_| | | | |_) | (_| | (__|   <| |___| (_| | || (__| | | |  __/ |   
  \_____\__,_|_|_|____/ \__,_|\___|_|\_\\_____\__,_|\__\___|_| |_|\___|_|   
                                                                            
This tool is a glorified honeypot to detect and record out-of-band connect backs
during a pentest. Based on Python's TCPServer module.
"""

from ConfigParser import ConfigParser, Error
from SocketServer import TCPServer
from SocketServer import UDPServer
from SocketServer import ThreadingMixIn
from handlers.basehandler import TcpHandler
from handlers.basehandler import UdpHandler
from logging.handlers import RotatingFileHandler
from OpenSSL import SSL

import xml.etree.ElementTree as ET
import sys
import socket
import os
import importlib
import logging
import threading
import thread
import multiprocessing
import time
import random
import string
import atexit
import ssl

class Config(object):
    '''
    Holds and sorts the configuration variables.
    '''
    def parse(self, filename):
        '''
        Load the config from the specified ini file. 
        '''
        try:
            config = ConfigParser()
            config.read(filename)
            
            self.ipaddress = config.get('Catcher', 'ipaddress')
            
            self.servicesfile = config.get('Files', 'services')       
            self.fingerprintsfile = config.get('Files', 'fingerprints')
            self.tokensfile = config.get('Files', 'tokens')
            self.cert = config.get('Files', 'cert')  
            self.certkey = config.get('Files', 'certkey')      

            self.logenabled  = config.getboolean('Debugging', 'Enable')
            self.loglocation = config.get('Debugging', 'Location')
            self.logsize     = config.get('Debugging', 'Size')
            self.logrotate   = config.get('Debugging', 'Rotate')
            
            self.validate()
            
            print "[+] Config loaded"
        except Exception, e:
            print "[-] Config not loaded: %s" % e
            sys.exit()
    
    def validate(self):
        '''
        validates the given config file
        '''
        #check files exist
        if not os.path.exists(self.servicesfile):
            raise Error('%s services file not found' % self.servicesfile)
        if not os.path.exists(self.tokensfile):
            open(self.tokensfile, 'a').close()
        if not os.path.exists(self.fingerprintsfile):
            raise Error('%s fingerprint file not found' % self.fingerprintsfile)
        
        #Validate the debug log details
        if self.logenabled == True:
            if not os.path.exists(self.loglocation):
                open(self.loglocation, 'a').close()

class ThreadedIPv6TCPServer(ThreadingMixIn, TCPServer):
    daemon_threads = True
    allow_reuse_address = True
    address_family = socket.AF_INET6
    
class ThreadedIPv6UDPServer(ThreadingMixIn, UDPServer):
    daemon_threads = True
    allow_reuse_address = True
    address_family = socket.AF_INET6
    max_packet_size = 2048

class ThreadedTCPServer(ThreadingMixIn, TCPServer):
    daemon_threads = True
    allow_reuse_address = True
    
class ThreadedUDPServer(ThreadingMixIn, UDPServer):
    daemon_threads = True
    allow_reuse_address = True
    max_packet_size = 2048

def open_port_listener(ip, p, lock, config, options):
    #unpack variables
    port = p['number']
    protocol = p['protocol']
    sslenabled = p['ssl']
    handlerfile = p['handler']
    ipv6 = options.IPV6
    Handler = None
    
    if handlerfile:
        try:
            handlerdir = os.path.join(os.getcwd(), 'handlers')
            modulebasename = os.path.basename(handlerdir)
            handlername = os.path.splitext(handlerfile)[0]
            plugin = importlib.import_module(modulebasename + '.' + handlername)
            Handler = getattr(plugin, handlername)
        except Exception, e:
            print "[-] Error loading handler file '%s'. Using default..." % handlername
            Handler = None
        print "[+] Using custom handler: '%s'" % handlerfile
    
    try:
        address = (ip, port)
        if ipv6:
            address = ('::1', port, 0, 0)
        
        if Handler:
            #start server with specific handler
            if protocol.lower() == 'udp' and ipv6 is True:
                server = ThreadedIPv6UDPServer(address, Handler)
            elif protocol.lower() == 'tcp' and ipv6 is True:
                server = ThreadedIPv6TCPServer(address, Handler)
            elif protocol.lower() == 'udp':
                server = ThreadedUDPServer(address, Handler)
            else:
                server = ThreadedTCPServer(address, Handler)
        else:
            #start server with standard handlers
            if protocol.lower() == 'udp' and ipv6 is True:
                server = ThreadedIPv6UDPServer(address, UdpHandler)
            elif protocol.lower() == 'tcp' and ipv6 is True:
                server = ThreadedIPv6TCPServer(address, TcpHandler)
            elif protocol.lower() == 'udp':
                server = ThreadedUDPServer(address, UdpHandler)
            else:
                server = ThreadedTCPServer(address, TcpHandler)
            
        #VALUES TO PASS TO BASE SERVER CLASS
        server.lock = lock
        server.tokens = load_tokens(config.tokensfile)
        server.fingerprints = load_fingerprints(config.fingerprintsfile)
        server.options = options
        server.config = config
        #####################################
        
        if sslenabled is True:
            #server.socket = ssl.wrap_socket(server.socket, certfile='ssl/ca.crt', keyfile='ssl/ca.key', server_side=True)
            ctx = SSL.Context(SSL.SSLv23_METHOD)
            ctx.use_privatekey_file('ssl/ca.key')
            ctx.use_certificate_file('ssl/ca.crt')
            server.socket = SSL.Connection(ctx, server.socket)
        thread = threading.Thread(target=server.serve_forever())
        thread.start()
    except Exception, e:
        if 'Address already in use' in e.args:
            print "[-] Service already bound on %i/%s" % (port, protocol)
        else:
            print "[-] Error starting on %i/%s" % (port, protocol)
            print "[-] Error: %s" % e
        return

def clean_up_processes():
    '''
    Run on exit of application
    '''
    try:
        for p in multiprocessing.active_children():
            p.terminate()
    except Exception, e:
        print "[-] Error: %s" % e
        
def setup_debug_logging(location, size, backup, verbose):
    """
    Enables the logging for the entire app
    """
    logger = logging.getLogger('callback')
    logger.setLevel(logging.DEBUG)
    handler = RotatingFileHandler(location, maxBytes=size, backupCount=backup)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    
    stdOutLog = logging.StreamHandler(sys.stdout)
    logger.addHandler(stdOutLog)
    if verbose == True:
        stdOutLog.setLevel(logging.DEBUG)
    else:
        stdOutLog.setLevel(logging.WARNING)

    formatter = logging.Formatter('[+] %(levelname)s - %(message)s')
    stdOutLog.setFormatter(formatter)
    logger.addHandler(stdOutLog)

def load_ports(portfile):
    '''
    returns a list of dict of ports
    '''
    tree = ET.parse(portfile)
    root = tree.getroot()
    ports = []
    count = 0
    for service in root:
        if int(service.find("enabled").text) == 1:
            port = {}
            port['number'] = int(service.find("port").text)
            port['protocol'] = service.find("protocol").text.lower()
            port['ssl'] = int(service.find("ssl").text)
            port['handler'] = service.find("handler").text
            count = count+1
            ports.append(port)
    print "[+] Services enabled:\t%i of %i" % (count, len(ports))
    if count == 0:
        print "[-] No services enabled :("
        sys.exit()
    return ports

def load_fingerprints(fingerprintsfile):
    '''
    returns a list of dict of fingerprint signatures
    '''
    tree = ET.parse(fingerprintsfile)
    root = tree.getroot()
    fingerprints = []
    count = 0
    
    for finger in root:
        #print finger.find("name").text
        fingerprint = {}
        fingerprint['name'] = finger.find("name").text
        fingerprint['probe'] = (finger.find("probe").text).decode('base64')
        count = count+1
        fingerprints.append(fingerprint)
    return fingerprints 

def generate_token():
    chars = string.ascii_uppercase + string.digits + string.ascii_lowercase
    size = 25
    tokenvalue = ''.join(random.choice(chars) for _ in range(size))
    return tokenvalue

def add_token(token, tokensfile):
    with open(tokensfile, "a") as tfile:
        tfile.write(token + "\n")
        
def load_tokens(tokensfile):
    tokens = []
    with open(tokensfile) as tfile:
        tokens = tfile.read().splitlines()
    return tokens
    
if __name__ == '__main__':
    from optparse import OptionParser
    logger = logging.getLogger('CBC')
    
    usage = '%prog [OPTIONS] (args)'
    parser = OptionParser(usage=usage, version=__version__)
    parser.add_option('-c', '--config', dest='CONFIG', type="string", default=".settings/config.ini",
                      help='load details from config.ini')
    parser.add_option('-t', '--token', dest='TOKEN', type="string", 
                      help='add a value to search for in each callback')
    parser.add_option('-r', '--random-token', dest='RANDOMTOKEN', action="store_true", default=False,
                      help='add a random generated token to search for')
    parser.add_option('-f', '--flush', dest='FLUSH', action="store_true", default=False,
                      help='remove all tokens, regenerate SSL certificates')
    parser.add_option('-6', '--ipv6', dest='IPV6', action="store_true", default=False,
                      help='run the services with ipv6 (experimental)')
    parser.add_option('-v', '--verbose', dest='VERBOSE', action="store_true", default=False,
                      help='outputs logging to stdout')
    (opts, args) = parser.parse_args()
    
    print NAME
    print "[+] CallBackCatcher CLI Version:\t" + __version__
    print "[+] Python Version:\t\t" + sys.version

    if opts.CONFIG == '.settings/config.ini':
        print "[+] Using the default config.ini. Have you set the listening IP address?"
        
    #Parse config
    config = Config()
    config.parse(opts.CONFIG)
    
    #Flush files
    if opts.FLUSH is True:
        open(config.tokensfile, 'w').close()
        print "[+] Token file emptied"
    
    #Add random token
    token = None
    if opts.RANDOMTOKEN is True:
        #Generate a unique token here
        token = generate_token()
        add_token(token, config.tokensfile)
        print "[+] Adding token:\t%s" % token
        
    #Add searchable values/tokens
    if opts.TOKEN:
        token = opts.TOKEN
        add_token(opts.TOKEN, config.tokensfile)
        print "[+] Adding token:\t%s" % token
    
    #Validate
    if opts.VERBOSE is True:
        print "[+] Verbose turned on"
        
    if config.logenabled is True:
        setup_debug_logging(config.loglocation, config.logsize, config.logrotate, opts.VERBOSE)
        print "[+] Debugging log:\t%s" % config.loglocation
        
    #print settings files
    print "[+] Service file:\t%s" % config.servicesfile
    print "[+] Fingerprint file:\t%s" % config.fingerprintsfile
    print "[+] Tokens file:\t%s" % config.tokensfile
    
    host = config.ipaddress
    ports = load_ports(config.servicesfile)
    jobs = []
    count = 0
    
    print "\n[+] Starting catcher on %s" % host
    try:      
        atexit.register(clean_up_processes) #Register clean up function to close processes cleanly
        for port in ports:
            try:
                lock = multiprocessing.Lock()
                #job = multiprocessing.Process(target=open_port_listener, args=(lock, host, port, opts.VERBOSE, tokens, fingerprints, opts.IPV6))
                job = multiprocessing.Process(target=open_port_listener, args=(host, port, lock, config, opts))
                job.daemon = True
                jobs.append(job)
                print "[+] Opening socket on %i/%s" % (port['number'], port['protocol'])
                job.start()
                count = count + 1
            except Exception, e:
                print "[-] Unable to start thread for port %i/%s" % (port['number'], port['protocol'])
                print "[-] ERROR - %s" % str(e)
        
        time.sleep(2)   
        print "[+] Total running ports: %s" %str(count)
        
        while multiprocessing.active_children() > 0:
            time.sleep(0.1)
    except KeyboardInterrupt:
        print "[+] ^C received, shutting down server"
        logger.info("Stopping CBC %s" % __version__)
        clean_up_processes()
        